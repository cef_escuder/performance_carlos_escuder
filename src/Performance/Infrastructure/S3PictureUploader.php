<?php

/**
 * Created by PhpStorm.
 * User: Carles
 * Date: 18/04/2017
 * Time: 18:00
 */
namespace Performance\Infrastructure;

use Aws\S3\S3Client;
use League\Flysystem\Filesystem;
use League\Flysystem\AwsS3v3\AwsS3Adapter;
use League\Flysystem\Config;
use Performance\Domain\PictureUploader;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class S3PictureUploader implements PictureUploader
{
    private $filesystem;

    public function __construct()
    {
        $client = new S3Client([
            'credentials' => [
                'key' => 'AKIAIWWZFYRRPQS7L6ZA',
                'secret' => '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',
            ],
            'region' => 'eu-west-1',
            'version' => 'latest',
        ]);

        $aws3adapter = new AwsS3Adapter($client, 'cefescudermpwar');

        $this->filesystem = new Filesystem($aws3adapter, new Config([]));
    }

    public function execute(UploadedFile $profilePicture, $username)
    {
        $name = 'profilePicture.jpg';
        if($profilePicture->getClientMimeType()=== 'image/jpeg'){
            $name = 'profilePicture.jpg';
        }else if($profilePicture->getClientMimeType()=== 'image/png'){
            $name = 'profilePicture.png';
        }else if($profilePicture->getClientMimeType()=== 'image/gif'){
            $name = 'profilePicture.gif';
        }

        $this->filesystem->write("/profilePicture/$username/$name", file_get_contents($profilePicture->getPathname()));

        return $name;
    }

}