<?php
/**
 * Created by PhpStorm.
 * User: Carles
 * Date: 18/04/2017
 * Time: 17:51
 */

namespace Performance\Domain\UseCase;


use Performance\Domain\Exception\NotValidFileException;
use Performance\Domain\PictureUploader;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class SaveProfilePicture
{
    const ACCEPTED_FILE_TYPES = ['image/jpeg', 'image/png', 'image/gif'];

    private $pictureUploader;

    public function __construct(PictureUploader $pictureUploader)
    {
        $this->pictureUploader = $pictureUploader;
    }

    public function execute(UploadedFile $profilePicture, $username)
    {
        if(!$this->isAValidImageType($profilePicture->getClientMimeType())){
            throw new NotValidFileException("the image you uploaded was not one of the required types.");
        }

        return  $this->pictureUploader->execute($profilePicture, $username);

    }

    private function isAValidImageType($imageType){
        return in_array($imageType,self::ACCEPTED_FILE_TYPES);
    }
}