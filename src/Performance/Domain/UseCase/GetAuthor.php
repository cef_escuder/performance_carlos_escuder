<?php
/**
 * Created by PhpStorm.
 * User: Carles
 * Date: 06/05/2017
 * Time: 13:43
 */

namespace Performance\Domain\UseCase;


use Performance\Domain\AuthorRepository;

class GetAuthor
{
    /**
     * @var AuthorRepository
     */
    private $authorRepository;

    public function __construct(AuthorRepository $authorRepository) {
        $this->authorRepository = $authorRepository;
    }

    public function execute($id) {
        return $author = $this->authorRepository->findOneById($id);
    }

}