<?php
/**
 * Created by PhpStorm.
 * User: Carles
 * Date: 18/04/2017
 * Time: 17:59
 */

namespace Performance\Domain;

use Symfony\Component\HttpFoundation\File\UploadedFile;

interface PictureUploader
{
    public function execute(UploadedFile $picture, $username);
}