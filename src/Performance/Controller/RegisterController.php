<?php

namespace Performance\Controller;

use Performance\Domain\UseCase\SaveProfilePicture;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Performance\Domain\UseCase\SignUp;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class RegisterController
{
    const DEFAULT_IMAGE_PATH = __DIR__ . "/defaultImage/images.jpg" ;
    const IMAGE_NAME = "images.jpg";
    const IMAGE_MIMETYPE = "image/jpeg";

    /**
     * @var \Twig_Environment
     */
    private $template;

    /**
     * @var UrlGeneratorInterface
     */
    private $url_generator;

    /**
     * @var SignUp
     */
    private $signUpuseCase;

    /**
     * @v*/
    private $saveProfilePictureUseCase;

    public function __construct(\Twig_Environment $templating, UrlGeneratorInterface $url_generator,
                                SignUp $signUpUseCase, SaveProfilePicture $saveProfilePictureUseCase) {
        $this->template = $templating;
        $this->url_generator = $url_generator;
        $this->signUpuseCase = $signUpUseCase;
        $this->saveProfilePictureUseCase = $saveProfilePictureUseCase;
    }

    public function get()
    {
        return new Response($this->template->render('register.twig'));
    }

    public function post(Request $request)
    {
    	$username = $request->request->get('username');
    	$password = $request->request->get('password');

        //added functionality, upload profile picture
        /** @var UploadedFile $profilePicture */
        $profilePicture = $request->files->get('profilePicture');

        if($profilePicture === null){
            $profilePicture = new UploadedFile(self::DEFAULT_IMAGE_PATH, self::IMAGE_NAME, self::IMAGE_MIMETYPE);
        }

        $pictureName = null;
        $pictureName = $this->saveProfilePictureUseCase->execute($profilePicture, $username);

        $this->signUpuseCase->execute($username, $password, $pictureName);

        return new RedirectResponse($this->url_generator->generate('login'));
    }
}