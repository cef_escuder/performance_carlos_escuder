<?php

namespace Performance\Controller;

use Performance\Domain\UseCase\GetAuthor;
use Symfony\Component\HttpFoundation\Response;
use Performance\Domain\UseCase\ListArticles;
use \Predis\ClientInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class HomeController
{
    /**
     * @var \Twig_Environment
     */
	private $template;

    /**
     * @var ListArticles
     */
	private $useCase;

    /**
     * @var ClientInterface
     */
	private $redis;

    /**
     * @var SessionInterface
     */
    private $session;

    /**
     * @var GetAuthor
     */
    private $getAuthorUseCase;

    public function __construct(\Twig_Environment $templating, ListArticles $useCase, ClientInterface $redis, SessionInterface $session, GetAuthor $getAuthorUseCase) {
        $this->template = $templating;
        $this->useCase = $useCase;
        $this->redis = $redis;
        $this->session = $session;
        $this->getAuthorUseCase = $getAuthorUseCase;
    }

    public function get()
    {
        if (!$articles = unserialize($this->redis->get("index:blog"))){
            $articles = $this->useCase->execute();
            $this->redis->set("index:blog", serialize($articles));
            $this->redis->expire("index:blog", 30);
        }

        $generalTop5Articles = $this->getGeneralTop5Articles();

        $userTop5articles = null;
        $author = null;
        if($this->session->get('author_id')){
           $userTop5articles = $this->getUserTop5Articles($this->session->get('author_id'));

           $id = $this->session->get('author_id');
           if(!$author = unserialize($this->redis->get("author:$id"))){
               $author = $this->getAuthorUseCase->execute($id);
               $this->redis->set("author:$id", serialize($author));
               $this->redis->expire("author:$id)", 30);
           }
        }

        return new Response($this->template->render(
                'home.twig',
                ['articles' => $articles,
                    'generalTop5' => $generalTop5Articles,
                    'userTop5' => $userTop5articles,
                    'author' => $author]),
                200,
                ['Cache-Control' => 's-maxage=3600, max-age=3600, public, no-cache']
            );
    }

    private function getGeneralTop5Articles(){
        $top5Articles =[];
        foreach ($this->redis->zrevrange("general:top:5",0,4) as $article){
            $top5Articles[]=unserialize($article);
        }
        return $top5Articles;
    }

    private function getUserTop5Articles($id){
        $top5Articles =[];
        foreach ($this->redis->zrevrange("$id:top:5",0,4) as $article){
            $top5Articles[]=unserialize($article);
        }
        return $top5Articles;
    }
}