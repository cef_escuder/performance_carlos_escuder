<?php

namespace Performance\Controller;

use Performance\Domain\UseCase\GetAuthor;
use Predis\ClientInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Performance\Domain\UseCase\ReadArticle;

class ArticleController
{
    /**
     * @var \Twig_Environment
     */
    private $template;

    /**
     * @var ReadArticle
     */
    private $useCase;

    /**
     * @var ClientInterface
     */
    private $redis;

    /**
     * @var SessionInterface
     */
    private $session;
    /**
     * @var GetAuthor
     */
    private $getAuthorUseCase;

    public function __construct(\Twig_Environment $templating, ReadArticle $useCase, ClientInterface $redis, SessionInterface $session, GetAuthor $getAuthorUseCase)
    {
        $this->template = $templating;
        $this->useCase = $useCase;
        $this->redis = $redis;
        $this->session = $session;
        $this->getAuthorUseCase = $getAuthorUseCase;
    }

    public function get($article_id)
    {
        if (!$article = unserialize($this->redis->get("article:$article_id"))) {
            $article = $this->useCase->execute($article_id);
            $this->redis->set("article:$article_id", serialize($article));
            $this->redis->expire("article:$article_id", 60);
        }

        if (!$article) {
            throw new HttpException(404, "Article $article_id does not exist.");
        }

        $this->saveArticleIntoGeneralTopList($article);

        $author = null;
        if($this->session->get('author_id')){
            $this->saveArticleIntoUserTopList($article, $this->session->get('author_id'));

            $id = $this->session->get('author_id');
            if(!$author = unserialize($this->redis->get("author:$id"))){
                $author = $this->getAuthorUseCase->execute($id);
                $this->redis->set("author:$id", serialize($author));
                $this->redis->expire("author:$id)", 30);
            }
        }

        return new Response($this->template->render(
            'article.twig',
            ['article' => $article,
                'author' => $author]),
            200,
            ['Cache-Control' => 's-maxage=3600, max-age=3600, public, no-cache']
            );
    }

    private function saveArticleIntoGeneralTopList($article)
    {
       $this->redis->zincrby("general:top:5",1,serialize($article));
    }

    private function saveArticleIntoUserTopList($article, $id){
        $this->redis->zincrby("$id:top:5",1,serialize($article));
    }
}